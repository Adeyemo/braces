var Braces = angular.module('Braces', []);

Braces.factory('Alert', function(){
	var obj = {};
	obj.successAlert = function(Message){

	}

	obj.confirmAlertData = {
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!',
			  cancelButtonText: 'No, cancel!',
			  confirmButtonClass: 'btn btn-success',
			  cancelButtonClass: 'btn btn-danger',
			  buttonsStyling: false
			}

	obj.errorAlert = function(){

	}
	return obj
})

Braces.controller('AppController', function($scope, $rootScope,Alert){
	
	$scope.contacts = [];

	$scope.showAdd = true;
	$scope.showEdit = false;
	$scope.showView = false;
	$scope.contacts = [];

	$scope.view = function(){
		$rootScope.forEdit = {}
		$scope.showEdit = false;
		$scope.showView = true;
		$scope.showAdd = false;
	}
	$scope.home = function(){
		$scope.showEdit = false;
		$scope.showView = false;
		$scope.showAdd = true;
	}

	$scope.addContacts = function(contact){
			$scope.contacts.push({
			"id": $scope.contacts.length+1,
			"first": contact.first,
			"last": contact.last,
			"phone": contact.phone
		});
		$scope.contact = {};
		swal(
		    'Added!',
		    'Your Contact has been Added.',
		    'success'
		);
		console.log($scope.contacts)
	}

	$scope.edit = function(contact){
		$scope.showAdd = false;
		$scope.showEdit = true;
		$rootScope.forEdit = contact
	}
	$scope.save = function(contact){
		angular.forEach($scope.contacts, function(value, key){
			if(value.id == contact.id){
				value = contact;
				swal(
				    'Saved!',
				    'Your Changes have been Saved.',
				    'success'
				);
				$rootScope.forEdit = {};
				$scope.view()
			}
		})
	}
	$scope.delete = function(id){
		swal(Alert.confirmAlertData).then(function() {
			angular.forEach($scope.contacts, function(value, key){
				if(value.id == id){
					$scope.contacts.splice($scope.contacts.indexOf(value),1);
					$scope.home();
					swal(
					    'Deleted!',
					    'Your Contact has been deleted.',
					    'success'
					 );

				}
			})
		}, function(dismiss) {
		  // dismiss can be 'cancel', 'overlay', 'close', 'timer'
		  if (dismiss === 'cancel') {
		    swal(
		      'Cancelled',
		      'The Contact was not Deleted',
		      'error'
		    );
		  }
		})
	}
})